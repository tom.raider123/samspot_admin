<x-app-layout>
<div class="container my-12 mx-auto px-4 md:px-12">
@include('flash-message')

			<!--Card-->
			 
<div id='recipients' class="p-8 mt-6 lg:mt-0 rounded shadow bg-white">
<!-- form start -->
@if($user)
{!! Form::model($user,[
            'route' => $user->exists ? ['users.update', $user->id] : 'users.store',
            'method' => $user->exists ? 'PUT' : 'POST',
            'id' => 'form_user_create',
            'files'=>true
            ]) !!}
<div class="flex flex-wrap -mx-3 mb-6">
  <div class="w-full md:w-1/2 px-3">
    <img class="rounded-full h-40 w-40 flex items-center justify-center	" src="{{$user->profile_picture ? $user->profile_picture : 'https://source.unsplash.com/random'}}" alt="">
    </div>
  </div>
  <div class="flex flex-wrap -mx-3 mb-6">
  <div class="w-full md:w-1/2 px-3">
      <label  class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-last-name">
        Name
      </label>
      <input  name="name" value="{{$user->name}}" class="appearance-none block w-full bg-gray-200 text-gray-700 border @error('name') border-red-500 @enderror border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-last-name" type="text" placeholder="Name">
      @error('name')
    <div class="text-red-500">{{ $message }}</div>
      @enderror
    </div>
    <div class="w-full md:w-1/2 px-3">
      <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-last-name">
        Email
      </label>
      <input readonly name="email" value="{{$user->email}}" class="appearance-none block w-full bg-gray-200 text-gray-700 border @error('email') border-red-500 @enderror border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-last-name" type="text" placeholder="Email">
      @error('email')
    <div class="text-red-500">{{ $message }}</div>
      @enderror
    </div>
  </div>
  <div class="flex flex-wrap -mx-3 mb-6">
  <div class="w-full md:w-1/2 px-3">
      <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-last-name">
      Gender
      </label>
      <div class="relative">
        <select name="gender" value="{{ old( 'gender', $user->gender) }}" class="block appearance-none w-full bg-gray-200 border @error('gender') border-red-500 @enderror border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-state">
        <option value="" >--Select Gender--</option>
          <option value="Male" {{ ($user->gender == 'Male') ? 'selected' : ''}}>Male</option>
          <option value="Female" {{ ($user->gender == 'Female') ? 'selected' : ''}}>Female</option>
          <option value="Any" {{ ($user->gender == 'Any') ? 'selected' : ''}}>Gender Neural</option>
        </select>
        @error('gender')
        <div class="text-red-500">{{ $message }}</div>
      @enderror
      </div>
    </div>
    <div class="w-full md:w-1/2 px-3">
      <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-last-name">
      Role
      </label>
      <input readonly value="{{$user->role_id === 2 ? 'User' : ''}}" class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-last-name" type="text" placeholder="Role">
    </div>
  </div>
  <div class="flex flex-wrap -mx-3 mb-6">
  <div class="w-full md:w-1/2 px-3">
      <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-last-name">
      Latitude
      </label>
      <input  readonly name="lat" value="{{$user->lat}}" class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-last-name" type="text" placeholder="Latitude">
      @error('lat')
    <div class="text-red-500">{{ $message }}</div>
      @enderror
    </div>
    <div class="w-full md:w-1/2 px-3">
      <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-last-name">
      Longitude
      </label>
      <input  readonly name="lng" value="{{$user->lng}}" class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-last-name" type="text" placeholder="Longitude">
      @error('lng')
    <div class="text-red-500">{{ $message }}</div>
      @enderror
    </div>
  </div>
  <div class="flex flex-wrap -mx-3 mb-6">
  <div class="w-full md:w-1/2 px-3">
  <a href="{{ url()->previous() }}" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">Back</a>
  
  {{ Form::submit('Submit', array('class' => 'bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline')) }}
      {{ Form::close() }}

  </div>
  </div>
</from>
@endif
<!-- form start end -->
</div>
<!-- card end -->
</div>
</x-app-layout>