<x-app-layout>
   <!-- tabel start -->
   <div class="container my-12 mx-auto px-4 md:px-12">
    <!-- alert start -->
    @include('flash-message')
<!-- alert end -->
			<!--Card-->
			 <div id='recipients' class="p-8 mt-6 lg:mt-0 rounded shadow bg-white">
      <div class="mb-5">
      <form class="" method="post" id="frmFilter">
  <div class="flex flex-wrap ">
    <div class="w-full md:w-1/5 px-5 mb-6 md:mb-0">
    <label for="datepicker" class="font-bold mb-1 text-gray-700 block">Select start date</label>
      <input name="start_date" class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-city" type="date" placeholder="Start date">
    </div>
    <div class="w-full md:w-1/5 px-5 mb-6 md:mb-0">
    <label for="datepicker" class="font-bold mb-1 text-gray-700 block">Select end date</label>
      <input name="end_date" class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-zip" type="date" placeholder="End date">
    </div>
    <div class="w-full md:w-1/5 px-5 mb-6 md:mb-0">
    <label for="datepicker" class="font-bold mb-1 text-gray-700 block">Select</label>
    <select name="is_verified"  class="block appearance-none w-full bg-gray-200 border  border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-state">
        <option value="" >--Select Gender--</option>
        <option value="1">Verified</option>
        <option value="0">Unverified</option>
        </select>
    </div>
    <div class="w-full md:w-1/5 px-5 mb-6 md:mb-0">
    <label for="datepicker" class="font-bold mb-1 text-gray-700 block">Action</label>
      <button class=" bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="submit">
        Filter
      </button>
      <button  onclick="resetFilter();" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="button">
        Reset
      </button>
    </div>
  </div>
</form>
      </div>
       
				<table id="users" class="stripe hover" style="width:100%; padding-top: 1em;  padding-bottom: 1em;">
					<thead>
						<tr>
              <th data-priority="1">S.no</th>
							<th data-priority="2">Email</th>
							<th data-priority="3">Name</th>
							<th data-priority="4">Gender</th>
              <th data-priority="5">IsActive</th>
              <th data-priority="6">Action</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
			<!--/Card-->
      </div>
      <!--/container-->
   <!-- table end -->
</x-app-layout>
<script>
$(document).ready(function(){
        $('#frmFilter :input:not(:button, [type="hidden"])').val('');
        getUser();
    });
    $('#frmFilter').submit(function(){
      getUser();
        return false;
    });
    function resetFilter(){
      $('#frmFilter :input:not(:button, [type="hidden"])').val('');
      getUser();
}

      function getUser(){
        var start_date = $('#frmFilter [name=start_date]').val();
        var end_date = $('#frmFilter [name=end_date]').val();
        var is_verified = $('#frmFilter [name=is_verified]').val();
        $('#users').dataTable().fnDestroy();
        $('#users tbody').empty()

	    var table = $('#users').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
                url: '{{ route('userslist') }}',
                method: 'GET',
                data: {  
                start_date:start_date,
                end_date:end_date,
                is_verified:is_verified
                }
            },
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'email', name: 'email'},
            {data: 'name', name: 'name'},
            {data: 'gender', name: 'gender'},
            {data: 'is_active', name: 'is_active'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
  }
 
	</script>
  <script>
  
  </script>
  