<x-app-layout>
    <div class="container my-12 mx-auto px-4 md:px-12">
    <div class="flex flex-wrap -mx-1 lg:-mx-4 ">

        <!-- Column -->
        <div class="my-1 px-1 w-full md:w-1/2 lg:my-4 lg:px-4 lg:w-1/3">

            <!-- Article -->
            <article class="overflow-hidden rounded-lg shadow-lg bg-white">
                <header class="flex items-center justify-between leading-tight p-2 md:p-4">
                    <h1 class="text-lg">
                    <span class="text-4xl text-green-400">
                         @if($register_user)
                           {{$register_user}}
                           @else
                           {{'0'}}
                           @endif
                    </span>
                    </h1>
                </header>

                <footer class="flex items-center justify-between leading-none p-2 md:p-4">
                    <a class="flex items-center no-underline hover:underline text-black" href="#">
                        <span class="text-gray-400 text-5xl"><i class="fas fa-user-friends"></i></span>
                        <p class="text-lg ml-2 text-sm">
                            Total registrations
                        </p>
                    </a>
                </footer>
            </article>
            <!-- END Article -->
        </div>
        <!-- END Column -->
        <!-- Column -->
        <div class="my-1 px-1 w-full md:w-1/2 lg:my-4 lg:px-4 lg:w-1/3">
            <!-- Article -->
            <article class="overflow-hidden rounded-lg shadow-lg bg-white">
                <header class="flex items-center justify-between leading-tight p-2 md:p-4">
                    <h1 class="text-lg">
                    <span class="text-4xl text-green-400">
                         @if($people_chatting)
                           {{$people_chatting}}
                           @else
                           {{'0'}}
                           @endif
                    </span>
                    </h1>
                </header>
                <footer class="flex items-center justify-between leading-none p-2 md:p-4">
                    <a class="flex items-center no-underline hover:underline text-black" href="#">
                        <span class="text-gray-400 text-5xl"><i class="fas fa-comments"></i></span>
                        <p class="text-lg ml-2 text-sm">
                            People chatting everyday
                        </p>
                    </a>
                </footer>
            </article>
            <!-- END Article -->
        </div>
        <!-- END Column -->

        <!-- Column -->
        <div class="my-1 px-1 w-full md:w-1/2 lg:my-4 lg:px-4 lg:w-1/3">
            <!-- Article -->
            <article class="overflow-hidden rounded-lg shadow-lg bg-white">
                <header class="flex items-center justify-between leading-tight p-2 md:p-4">
                    <span class="text-4xl text-green-400">
                         @if($friend_request)
                           {{$friend_request}}
                           @else
                           {{'0'}}
                           @endif
                    </span>
                </header>
                <footer class="flex items-center justify-between leading-none p-2 md:p-4">
                    <a class="flex items-center no-underline hover:underline text-black" href="#">
                        <span class="text-gray-400 text-5xl"><i class="fas fa-link"></i></span>
                        <p class="text-lg ml-2 text-sm">
                            Friend request per day
                        </p>
                    </a>
                </footer>

            </article>
            <!-- END Article -->

        </div>
        <!-- END Column -->
    </div>
</div>
</x-app-layout>
