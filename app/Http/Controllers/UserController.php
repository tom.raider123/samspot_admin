<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use DataTables;
use Form;
use Auth;
use Validator;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            return view('users.users');
        }catch (\Exception $e){
            return redirect()->back()->with('error',$e->getMessage());
        }
    }

    public function getUsers(Request $request)
    {
        try{
            if($request->ajax()) {
                $start_date = $request->input('start_date');
                $end_date = $request->input('end_date');
                $is_verified = $request->input('is_verified');
                $query = User::select('*')->where('role_id','!=',1); 
                if($start_date !='' && $end_date !='') {
                    $query->whereDate('created_at', '>=', $start_date)
                ->whereDate('created_at', '<=', $end_date);
                }
                if($is_verified !='') {
                    $query->where('is_verified',$is_verified);
                }
                $data = $query->get();
                return Datatables::of($data)
                    ->addIndexColumn() 
                    ->addColumn('is_active', function($row){
                        if($row->is_active == TRUE ){
                            return "<a href='".route('users.status',$row->id)."'><span class='inline-flex items-center justify-center px-2 py-1 text-xs font-bold leading-none text-green-100 bg-green-500 rounded'>Active</span></a>";
                        }else{
                            return "<a href='".route('users.status',$row->id)."'><span class='inline-flex items-center justify-center px-2 py-1 text-xs font-bold leading-none text-red-100 bg-red-500 rounded'>Inactive</span></a>";
                        }
                    })
                    ->addColumn('action', function($row){
                        return
                        // view
                        '<a href="'.route('users.show',[$row->id]).'" class="mr-2 text-blue-500"><i class="fas fa-eye"></i></a> '.
                        '<a href="'.route('users.edit',[$row->id]).'" class="mr-2 text-gray-500"><i class="fas fa-user-edit"></i>'.
                        // Delete
                        Form::open(array(
                            'style' => 'display: inline-block;',
                            'method' => 'DELETE',
                            'onsubmit'=>"return confirm('Do you really want to delete?')",
                            'route' => ['users.destroy', $row->id])).
                        '<button type="submit" class="ml-2 text-red-500"><i class="fas fa-trash-alt"></i></button>'.
                        Form::close();
                    })
                    ->rawColumns(['action', 'profile_picture','is_active'])
                    ->make(true);
            }
        }catch (\Exception $e){
            return redirect()->back()->with('error',$e->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($user_id)
    {
        $user = User::findOrFail($user_id); 
        return view('users.view',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id); 
        return view('users.edit',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        try{
           
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'gender' => 'required',
            ]); 
            if ($validator->fails()) {
                return redirect()->back()
                            ->withErrors($validator)
                            ->withInput();
            }

        $data = $request->all();
        $type_image = $request->file('image');
        if($type_image){
            $path = public_path(config('constants.SETTING_IMAGE_URL'));
            $type_imageName = time().'.'.$type_image->getClientOriginalExtension();
            File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);
            $uploadResponse = $type_image->move($path, $type_imageName);
            $data['image'] = $type_imageName;
        }
        $user->update($data);   
        return redirect()->back()->with('success', 'User updated successfully');
    }catch (\Exception $e){
        return redirect()->back()->with('error',$e->getMessage());
    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $user = User::find($id); 
            $user->delete();
            return redirect()->back()->with('success','User deleted successfully');
        }catch (\Exception $e){
            return redirect()->back()->with('error',$e->getMessage());
        }
    }

    public function status(Request $request, $id=null){
        $user = User::findOrFail($id);
        if (isset($user->is_active) && $user->is_active==FALSE) {
            $user->is_active = TRUE;
            $user->save();
            $request->session()->flash('success','user activated successfully');
        }else{
            $user->is_active = FALSE;
            $user->save();
            $request->session()->flash('warning','user deactivated successfully');
        }
        return redirect()->back();
    }
}
