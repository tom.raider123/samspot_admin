<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Friendship;
use Auth;

class DashboardController extends Controller
{
    
    
    public function index()
    {
        try{
            $data['register_user'] = User::all()->except(Auth::id())->count();
            $data['friend_request'] = round(Friendship::all()->count() / 30, 2);
            $data['people_chatting'] = 0;
            return view('dashboard',$data);
        }catch (\Exception $e){
            return redirect()->back()->with('error',$e->getMessage());
        }
    }
}
