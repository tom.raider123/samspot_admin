<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; 

class Trash extends Model
{
    use HasFactory;
    protected $fillable = [
        'deleted_by',
        'deleted_user_id',
    ];

    protected $dates = [ 'deleted_at' ];

    public function getDeletedUser() {
        return $this->belongsTo(User::class, 'deleted_user_id','id');
    }
}
