<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserSettings extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 'want_to_see', 'show_my_profile_to', 'is_invisible', 'is_show_nearbuy_people'];
}
