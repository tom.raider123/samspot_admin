<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Http\Traits\Friendable;

class Visitor extends Model
{
    use HasFactory, Friendable;

    protected $table = "recent_visitor";
    protected $fillable = [
        'visitor_id',
        'visited_id'
    ];

    public function getVisitedUser() {
        return $this->belongsTo(User::class, 'visited_id','id');
    }
}
